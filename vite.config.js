import legacyPlugin from '@vitejs/plugin-legacy';
import pugPlugin from '@vituum/vite-plugin-pug';
import postcssAutoprefixer from 'autoprefixer';
import { readFileSync, unlink } from 'fs';
import { globSync } from 'glob';
import { posix } from 'path';
import postcssMergeLonghand from 'postcss-merge-longhand';
import postcssPresetEnv from 'postcss-preset-env';
import postcssSortMediaQueries from 'postcss-sort-media-queries';
import { defineConfig } from 'vite';
import { ViteImageOptimizer as imageOptimizerPlugin } from 'vite-plugin-image-optimizer';
import sitemapPlugin from 'vite-plugin-sitemap';
import createSvgSpritePlugin from 'vite-plugin-svg-spriter';
import vituumPlugin from 'vituum';

import packageJson from './package.json';

const VITE_SETTINGS = {
	hostname: 'http://localhost/',
	isHashed: true,
	isLegacyEnabled: false,
	minifyType: 'esbuild',
	scriptFromPublic: false,
	staging: process.env.STAGING_ENV,
};

export default defineConfig(({ mode }) => {
	return {
		appType: 'mpa',

		base: './',

		build: {
			assetsInlineLimit: 0,
			chunkSizeWarningLimit: '1024',
			emptyOutDir: true,
			minify: VITE_SETTINGS.minifyType,
			outDir: 'build',
			rollupOptions: {
				output: {
					assetFileNames: ({ names }) => {
						let folder = '';

						if (names?.length) {
							const [name] = names;

							if (/\.(jpe?g|png|gif|tiff|webp|svg|avif)$/i.test(name ?? '')) {
								folder = 'images';
							} else if (/\.css$/.test(name ?? '')) {
								folder = 'styles';
							} else if (/\.(woff2?|ttf|otf|eot)$/.test(name ?? '')) {
								folder = 'fonts';
							}
						}

						return `assets/${folder ? `${folder}/` : ''}[name]${VITE_SETTINGS.isHashed ? '-[hash]' : ''}[extname]`;
					},
					chunkFileNames: `assets/scripts/[name]${VITE_SETTINGS.isHashed ? '-[hash]' : ''}.js`,
					entryFileNames: `assets/scripts/[name]${VITE_SETTINGS.isHashed ? '-[hash]' : ''}.js`,
					manualChunks: (id) => {
						if (!VITE_SETTINGS.scriptFromPublic) {
							if (id.includes('node_modules')) {
								return 'vendor-scripts';
							}
						}

						return null;
					},
				},
			},
		},

		clearScreen: false,

		css: {
			postcss: {
				plugins: [
					postcssSortMediaQueries({
						sort: 'desktop-first',
					}),

					...(mode === 'production'
						? [postcssPresetEnv(), postcssMergeLonghand(), postcssAutoprefixer({ cascade: true, grid: 'autoplace' })]
						: []),
				],
			},
		},

		plugins: [
			vituumPlugin({
				imports: {
					filenamePattern: {
						'+.css': [],
						'+.js': './src/scripts',
						'+.scss': './src/styles',
					},
					paths: ['./src/styles/pages/*', './src/scripts/pages/*'],
				},
				pages: {
					normalizeBasePath: true,
				},
			}),
			pugPlugin({
				globals: {
					isDevelopment: mode === 'development',
				},
				root: './src',
			}),
			createSvgSpritePlugin({
				svgFolder: './src/assets/icons/',
				svgSpriteConfig: {
					shape: {
						transform: [
							{
								svgo: {
									plugins: [
										'preset-default',
										{
											active: true,
											name: 'removeAttrs',
											params: {
												attrs: '(fill|fill-opacity|fill-rule|stroke|stroke-width|stroke-linecap|stroke-linejoin|stroke-opacity|opacity|clip-rule)',
											},
										},
										{
											active: true,
											name: 'removeDimensions',
										},
										{
											active: false,
											name: 'removeViewBox',
										},
										{
											active: true,
											name: 'inlineStyles',
											params: {
												onlyMatchedOnce: false,
											},
										},
									],
								},
							},
						],
					},
				},
				transformIndexHtmlTag: {
					injectTo: 'body-prepend',
				},
			}),
			imageOptimizerPlugin({
				avif: {
					quality: 95,
				},
				jpeg: {
					progressive: true,
					quality: 95,
				},
				png: {
					compressionLevel: 7,
					progressive: true,
					quality: 95,
				},
				svg: {
					multipass: true,
					plugins: [
						{
							name: 'preset-default',
							params: {
								cleanupIDs: {
									minify: false,
									remove: false,
								},
								convertPathData: false,
								overrides: {
									cleanupNumericValues: false,
									removeViewBox: false,
								},
							},
						},
						'sortAttrs',
						{
							name: 'addAttributesToSVGElement',
							params: {
								attributes: [{ xmlns: 'http://www.w3.org/2000/svg' }],
							},
						},
					],
				},
				webp: {
					mixed: true,
					quality: 95,
				},
			}),
			sitemapPlugin({
				exclude: ['/page-list.html'],
				hostname: VITE_SETTINGS.hostname,
				outDir: 'build',
				robots:
					VITE_SETTINGS.staging === 'development'
						? [{ disallow: '/', userAgent: '*' }]
						: [{ allow: '/', userAgent: '*' }],
			}),
			VITE_SETTINGS.isLegacyEnabled && legacyPlugin(),
			{
				name: 'html-transform',
				transformIndexHtml(html) {
					let newContent = html.replaceAll(
						/(?:^|[^а-яёА-ЯЁ0-9_])(в|без|а|до|из|к|я|на|по|о|от|перед|при|через|с|у|за|над|об|под|про|для|и|или|со|около|между)(?:^|[^а-яёА-ЯЁ0-9_])/g,
						(match) => {
							return match.slice(-1) === ' ' ? `${match.substr(0, match.length - 1)}&nbsp;` : match;
						},
					);

					if (newContent.includes('page-list')) {
						newContent = newContent.replaceAll(
							'#project-name',
							packageJson.name
								.replace(/-|_/gm, ' ')
								.toLowerCase()
								.trim()
								.split(' ')
								.map((word) => `${word[0].toUpperCase()}${word.slice(1)}`)
								.join(' '),
						);
						const pugPages = globSync(`./src/pages/**/*.pug.json`).map((filePath) => {
							const { dir, name } = posix.parse(filePath);
							const splittedDir = dir.split('/');
							const pageFolder = splittedDir.at(-1) !== 'pages' ? splittedDir.at(-1) : '';
							const pageSlug = name.replace('.pug', '');

							let pageTitle = `${pageSlug.replace('.pug', '')} Page`;
							const dataFile = readFileSync(filePath, { encoding: 'utf8' });

							if (dataFile) {
								const parsedFile = JSON.parse(dataFile);

								if (parsedFile?.Settings?.title) {
									pageTitle = parsedFile.Settings.title;
								}
							}

							return { pageFolder, pageSlug, pageTitle };
						});
						let pageListString = '<ol class="section-page-list__items">';

						pugPages.forEach(({ pageFolder, pageSlug, pageTitle }) => {
							if (!pageSlug.includes('page-list')) {
								const linkText = pageTitle
									.split(' ')
									.map((word) => `${word[0].toUpperCase()}${word.slice(1)}`)
									.join(' ')
									.trim();
								const linkUrl =
									pageSlug === 'index'
										? `./${pageFolder ? `${pageFolder}/` : pageFolder}`
										: `${pageFolder ? `/${pageFolder}` : pageFolder}/${pageSlug}.html`;

								pageListString += `<li class="section-page-list__item"><a href="${linkUrl}" class="section-page-list__link" target="_blank">${linkText}</a></li>`;
							}
						});

						pageListString += '</ol>';

						newContent = newContent.replaceAll(/<div id="section-page-list"><\/div>/g, pageListString);
					}

					return newContent;
				},
			},
			VITE_SETTINGS.scriptFromPublic && {
				closeBundle() {
					if (mode === 'production') {
						const devScriptsToRemove = globSync('build/assets/scripts/**/*.dev?(-*).js');

						if (devScriptsToRemove?.length) {
							devScriptsToRemove.forEach((devScriptToRemove) => {
								unlink(devScriptToRemove, (err) => {
									if (err) {
										throw err;
									}
								});
							});
						}
					}
				},
				name: 'remove-dev-scripts',
			},
		],

		server: {
			host: true,
			open: '/page-list.html',
		},
	};
});
