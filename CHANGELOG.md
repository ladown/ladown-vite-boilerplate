# 1.0.0 (2024-12-11)

### Bug Fixes

- **core-video:** fix variable naming ([7369826](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/7369826c39fb76d5c8d2601d2e0fe71da4fe52ab))
- **eslint.config.mjs:** disable options for eslint-plugin-import ([4a879f5](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/4a879f56c6c7df482a4df1a430783051aaddf155))
- **eslint.config.mjs:** remove import and import/typescript ([329b181](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/329b181d39962935ee4f7e510d838c4e193751aa))
- **script-utils:** fix cyrcle dependencies for setRootHeightVariables ([1c99660](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/1c99660349fe539827230730bfe5bf3cdccdb7a7))

### Features

- **button-burger:** integrate getClassNamesFiltered ([3af7860](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/3af78609d9b4ba87a7cb28ad273a2a34ea059a2f))
- **commit:** integrated commitizen, lint-staged to project ([06effdc](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/06effdc382b959cdaffb3c943bf2169aebb4b2ea))
- **components-loader:** update files according new components ([04d673f](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/04d673f1b9bdc7f170c66439477a765b002f6a32))
- **components/templates:** create folder ([aeebf16](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/aeebf16db46ebb82430878c4405b83a8d9939fc7))
- **core-modal:** avoid using additional class for modal trigger needs only modal-id attribute ([6ebf267](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/6ebf267039c99168b2af44ae439eb785196d173d))
- **core-modal:** integrate getClassNamesFiltered ([8e22481](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/8e224811883f90bc080054e19e7f692289d2b7f6))
- **core-photo:** integrated getClassNamesFiltered ([fb69810](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/fb698101ae316a79add2da082137756ae168aa3e))
- **core-svg-icon:** integrated getClassNamesFiltered ([92d34f2](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/92d34f2a8e34da5c768c69f1ad9416af4173131c))
- **core-svg-icon:** make viewBox as unnessacery atribute ([cc5491f](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/cc5491fcdd46214f58420822ff1f1e5c06789725))
- **core-video:** integrated getClassNamesFiltered ([4c9887a](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/4c9887aee9393a443acd33d91a2f8eca549ac52e))
- **default-layout:** create function getClassNamesFiltered for set modifiers and classNames ([ff49307](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/ff49307a867b7d03c9be83ca681773b691f3077b))
- **default-layout:** make output section and modals dynamic ([3b22f7e](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/3b22f7e3d3e56ff57788c54b23b233ebca42b559))
- **normalize.scss:** update normalize for styles ([dd8047b](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/dd8047bcd00c5bf6c0af2ad9e6576ad47551039a))
- **part-breadcrumbs:** integrated getClassNamesFiltered ([28230e0](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/28230e0443c492ebe6075b9b46a1058a2a4f3cef))
- **part-breadcrumbs:** rename from part-breadcrumb to part-breadcrumbs ([1439708](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/1439708b6e93d21407b96ee082375913257de5dd))
- remove slider folder in components ([8bba24c](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/8bba24c0099b72c6d401c8370ad7b0b0723b5a3d))
- replace form-field with ui-input ([224ab74](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/224ab74bf55edfa95aaf8000162c1d6c96f91f88))
- replace modules with core components ([d7b1316](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/d7b1316d5070c00597312cec7e1bb7f436ea7613))
- **script-constants:** rework structure for script consanst make them as signle file ([74aa667](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/74aa66739c88b6c47ff67211bd22653beac523c5))
- **script-utils:** add waitElementToBeAppend to utils ([c8de4eb](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/c8de4eb04a038cb191a20799490ebf8deb7dac9f))
- **script-utils:** update utils structure and make them as single file ([517c34e](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/517c34e2fa7038075e4dbf88a3ad5ba57f328c81))
- **scripts-utils:** add sanitizeHTML for sanitizing html ([9474c29](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/9474c291eaaa2fe2a5bf200ad5805ddf29a8d7dd))
- **smooth-scroll.js:** make it more native ([1150b90](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/1150b90064c51fa26d4f88043df6ede42dcd2a4b))
- **style-mixins:** add normalize-input-autocomplete-styles mixin ([867af5a](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/867af5a1925e4afc54f68d9ada6c77078db483ba))
- **style-mixins:** remove useless mixin ([e0d4ea5](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/e0d4ea59072178696e54b95149e1bb877517dc2c))
- **stylelint-config:** allow to keep empty style files ([b6df73d](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/b6df73deef95d5386087bb8b25b4c69ded7d66d0))
- **ui-button:** improve props and style order ([f18d94c](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/f18d94c23831725df5de0a04cd0b1ae292bb1e0d))
- **ui-button:** integrated getClassNamesFiltered ([203db8c](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/203db8c7fc4b569b69eee5d5bfbc69c3306a1fd1))
- **ui-checkbox:** add new component ([7144741](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/7144741a5e2a2285cbca32de94e5716ab0a50b7f))
- **ui-checkbox:** integrated getClassNamesFiltered ([c1c3704](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/c1c370451a204a7880261fb8ec5c1997e11f38b2))
- **ui-input:** integrated getClassNamesFiltered ([33b9fb5](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/33b9fb538e54b594b43db3e33f614663f5eba026))
- **vite.config.js:** Add option to enable @vitejs/plugin-legacy plugin ([1b19223](https://gitlab.com/ladown/ladown-vite-boilerplate/commit/1b1922369f7198733aac40927ca64037e003283e))

## 0.0.7 (04.08.2024)

- Up dependencies
- Add dynamic output for page sections
- Improve `stylelint`
- Up dependencies, update `eslint` configuration file and set `packageManager` to `package.json`
- Migrate to `eslint@9.8.0`
- Add option to enable `@vitejs/plugin-legacy` plugin
- Integrated `commitizen`

## 0.0.6 (02.07.2024)

- Up dependencies
- Add `"module": "es6"` setting to `package.json`
- Fix problem when page parts don't show when use mixins
- Remove `packageManager` field from `package.json`
- Up node version (`engines.node`) in `package.json`
- Minor fixes of files (layout/styles/scripts)
- Fix staging for build
- Add options to include script from `/public` to `vite.config.js` and add option to remove dev files on build
- Fix `package.json` scripts for formatters and linters

## 0.0.5 (21.04.2024)

- Up dependencies
- Fix `role="main"` for `<main></main>`
- Add `minifyType` option to `vite` config and rename `buildFormatted` -> `prettierBuild` for formatting build directory
- Add `part-templates` for templates to use in JavaScript, clean useless classes and check if slots is not empty for layouts parts - `part-header`, `part-main-content`, `part-tooltips`, `part-modals` and `part-templates`
- Change layout path for pages
- Install plugin `vite-plugin-sitemap` and patch it to generate `sitemap.xml` and `robots.txt`. For `robots.txt` add staging to hide from search bots site for development deploy

## 0.0.4 (13.04.2024)

- Up dependencies
- Rework component system and make it more like BEM structure. Component folder now keeps component style/script/layout
- Rework some components
- Add folder for icons
- Remove `splitVendorChunkPlugin` from `vite.config.js`
- Remove `stylelint-config-standard-vue/scss` from `.stylelintrc`

## 0.0.3 (19.02.2024)

- Up dependencies
- Update husky initialization and add new script to `package.json` for compatibility
- Fix order of plugins for correct work of `@vitejs/plugin-legacy`
- Update `README.md`
- Make minor updates of styles
- Remove aliases
- Rework src folder structure according to `vituum` plugin

## 0.0.2 (25.01.2024)

- Up dependencies
- Add `@gitlab/stylelint-config` to `.stylelintrc`
- Make minor updates of styles
- Clean pug components and make minor updates
- Add `assetsInlineLimit: 0` to prevent inline `.svg` in `.pug` files
- Replace `vite-plugin-svg-sprite` with `vite-plugin-svg-spriter`

## 0.0.1 (06.01.2024)

- Initial public release
