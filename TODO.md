# ladown-vite-boilerplate

Its vite boilerplate for projects

### In progress features ↺

-   [ ] Add possibility to create webp images with query parameter `?webp`. Waiting for this functionality in `vite-plugin-image-optimizer`.
-   [ ] Rework importing styles and scripts for components
-   [ ] Figure out is it possible to handle errors in pug and with missed images more informative
-   [ ] Auto-import icons to pug to reuse it

### Completed features ✓

-   [x] Update `@gitlab/stylelint-config` dependency to be compatible with `stylelint@16.1.0`
-   [x] Add possibility to disable hash and minification for output files
-   [x] Add sitemap generation for build
-   [x] Add robots for dev staging and prod
-   [x] In page-list display page title instead of file name
-   [x] Integrate `Commitizen`
-   [x] Update `eslint`,its plugins and configs to latest version

### Refused features ✖

-   Add page folders in `/src/pages/` to keep there page styles/scripts/data
-   Auto-import all used styles/script in page
