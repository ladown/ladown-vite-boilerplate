import babelParser from '@babel/eslint-parser';
import { fixupConfigRules } from '@eslint/compat';
import { FlatCompat } from '@eslint/eslintrc';
import js from '@eslint/js';
import eslintConfigPrettier from 'eslint-config-prettier';
import eslintPluginPerfectionist from 'eslint-plugin-perfectionist';
import eslintPluginPrettierRecommended from 'eslint-plugin-prettier/recommended';
import globals from 'globals';
import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const compat = new FlatCompat({
	allConfig: js.configs.all,
	baseDirectory: __dirname,
	recommendedConfig: js.configs.recommended,
});

export default [
	{
		ignores: ['**/node_modules/', '**/public/', '**/tmp/', '**/vendor/', '**/vendor.js', '**/build/'],
	},
	...fixupConfigRules(compat.extends('airbnb')),
	eslintPluginPerfectionist.configs['recommended-natural'],
	eslintConfigPrettier,
	eslintPluginPrettierRecommended,
	{
		languageOptions: {
			ecmaVersion: 2018,
			globals: {
				...globals.browser,
				...globals.node,
			},
			parser: babelParser,
			parserOptions: {
				ecmaFeatures: {
					impliedStrict: true,
				},
				requireConfigFile: false,
			},
			sourceType: 'module',
		},

		rules: {
			'class-methods-use-this': 'off',
			'import/extensions': ['error', 'ignorePackages'],
			'import/no-extraneous-dependencies': [
				'error',
				{
					devDependencies: true,
				},
			],
			// TODO: Turn on next two lines, when airbnb will migrate to eslint@9.0.0 and higher
			'import/no-named-as-default': 'off',
			'import/no-named-as-default-member': 'off',
			'import/no-unresolved': 'off',
			'import/prefer-default-export': 'off',
			'no-console': 'off',
			'no-debugger': ['off'],
			'no-param-reassign': [
				'error',
				{
					props: false,
				},
			],
			'no-plusplus': [
				'error',
				{
					allowForLoopAfterthoughts: true,
				},
			],
			'no-underscore-dangle': ['error', { allow: ['_id', '__filename', '__dirname'] }],
			'no-unused-vars': ['warn'],
			'perfectionist/sort-array-includes': [
				'error',
				{
					groupKind: 'literals-first',
					order: 'asc',
					type: 'natural',
				},
			],
			'perfectionist/sort-classes': [
				'error',
				{
					groups: [
						'constructor',
						'static-block',
						'static-property',
						'static-method',
						['protected-property', 'protected-accessor-property'],
						'protected-method',
						['private-property', 'private-accessor-property'],
						'private-method',
						['get-method', 'set-method'],
						'index-signature',
						'method',
						['property', 'accessor-property'],
						'unknown',
					],
				},
			],
			'perfectionist/sort-jsx-props': [
				'error',
				{
					groups: ['unknown', 'shorthand', 'multiline'],
				},
			],
			'prettier/prettier': ['error'],
		},
	},
];
