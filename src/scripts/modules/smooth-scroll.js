import getElementDocumentCoords from '../../../scripts/shared/utils/getElementDocumentCoords.js';

const SmoothScroll = {
	data: {
		elements: {
			buttons: Array.from(document.querySelectorAll('a[href*="#"]'))?.filter(
				(link) => link?.getAttribute('href')?.length > 1,
			),
			partHeader: document.querySelector('.js-part-header'),
		},
	},

	handlers: {
		handleButtonClick(event) {
			event.preventDefault();

			const { currentTarget } = event;
			const scrollTarget = currentTarget.getAttribute('href');
			const scrollElement = document.querySelector(scrollTarget);

			if (scrollElement) {
				this.methods.scrollToElement.call(this, scrollElement);
			}
		},
	},

	init() {
		if (this.data.elements.buttons?.length) {
			this.data.elements.buttons.forEach((button) => {
				this.listeners.setButtonClickListener.call(this, button);
			});
		}
	},

	listeners: {
		setButtonClickListener(button) {
			button.addEventListener('click', this.handlers.handleButtonClick.bind(this));
		},
	},

	methods: {
		scrollToElement(element) {
			const elementOffsetTop = getElementDocumentCoords(element).top;
			const scrollDistance = elementOffsetTop - (this.data.elements.partHeader?.offsetHeight || 0);

			window.scrollTo({ behavior: 'smooth', top: scrollDistance });
		},
	},
};

export default SmoothScroll;
