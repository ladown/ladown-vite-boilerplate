export default (number, divider = ' ') => {
	return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, divider);
};
