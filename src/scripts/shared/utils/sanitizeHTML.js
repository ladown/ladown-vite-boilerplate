import sanitizeHtml from 'sanitize-html';

export default (html) => {
	return typeof html === 'string'
		? sanitizeHtml(html, {
				allowedAttributes: {
					...sanitizeHtml.defaults?.allowedAttributes,
					iframe: ['src', 'class', 'title'],
					source: ['srcset', 'media'],
				},
				allowedTags: sanitizeHtml.defaults.allowedTags?.concat(['img', 'iframe', 'picture', 'source']),
			})
		: '';
};
