export default (element, callback) => {
	function fireCallBack(e) {
		if (e.target !== element) return;
		callback.call(element, e);
		element.removeEventListener('transitionend', fireCallBack);
	}
	if (callback) {
		element.addEventListener('transitionend', fireCallBack);
	}
};
