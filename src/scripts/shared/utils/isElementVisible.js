export default (element) => {
	return !!(element.offsetWidth || element.offsetHeight || element.getClientRects().length);
};
