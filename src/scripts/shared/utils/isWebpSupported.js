export default () => {
	const canvasElement = document.createElement('canvas');

	return (
		!(!canvasElement.getContext || !canvasElement.getContext('2d')) &&
		canvasElement.toDataURL('image/webp').indexOf('data:image/webp') === 0
	);
};
