export default (selector) => {
	return new Promise((resolve) => {
		const elementOnInitialization = document.querySelector(selector);

		if (elementOnInitialization) {
			resolve(elementOnInitialization);
		} else {
			const observer = new MutationObserver(() => {
				const element = document.querySelector(selector);

				if (element) {
					observer.disconnect();
					resolve(element);
				}
			});

			observer.observe(document.body, {
				childList: true,
				subtree: true,
			});
		}
	});
};
