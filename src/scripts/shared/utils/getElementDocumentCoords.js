export default (el) => {
	const element = el.getBoundingClientRect();

	return {
		bottom: element.bottom + window.scrollY,
		left: element.left + window.scrollX,
		right: element.right + window.scrollX,
		top: element.top + window.scrollY,
	};
};
