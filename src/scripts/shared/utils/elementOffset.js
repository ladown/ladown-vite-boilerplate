export default (element) => {
	const box = element.getBoundingClientRect();
	const { body } = document;
	const clientTop = element.clientTop || body.clientTop || 0;
	const clientLeft = element.clientLeft || body.clientLeft || 0;
	const scrollTop = element === window ? window.scrollY : element.scrollTop;
	const scrollLeft = element === window ? window.scrollX : element.scrollLeft;

	return {
		left: box.left + scrollLeft - clientLeft,
		top: box.top + scrollTop - clientTop,
	};
};
