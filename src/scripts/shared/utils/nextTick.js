export default (callback, delay = 0) => {
	return setTimeout(callback, delay);
};
