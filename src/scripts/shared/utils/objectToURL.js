export default (object) => {
	const pairs = [];

	Object.keys(object).forEach((objectKey) => {
		if (object[objectKey]) {
			pairs.push(`${encodeURIComponent(objectKey)}=${encodeURIComponent(object[objectKey])}`);
		}
	});

	return pairs.join('&');
};
