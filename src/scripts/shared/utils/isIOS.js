export default () => {
	return /iPad|iPhone|iPod/.test(navigator.userAgent);
};
