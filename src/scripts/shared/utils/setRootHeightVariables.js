import elementsToTrackHeight from '../constants/elementsToTrackHeight.js';
import getObjectLength from './getObjectLength.js';

export default (elements = elementsToTrackHeight) => {
	const documentRoot = document.getElementsByTagName('html')[0];

	if (getObjectLength(elements)) {
		Object.keys(elements).forEach((key) => {
			if (elements[key]) {
				const elementHeigh = Math.ceil(elements[key].offsetHeight);

				documentRoot.style.setProperty(`--${key}-height`, `${elementHeigh}px`);
			}
		});
	}
};
