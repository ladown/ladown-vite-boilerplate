export default () => {
	return (
		typeof window !== 'undefined' &&
		('ontouchstart' in window || window.navigator.maxTouchPoints > 0 || window.matchMedia('(pointer: coarse)').matches)
	);
};
