import EventEmitter from 'eventemitter3';

class App {
	static cssVariables = {
		'breakpoint-hd': null,
		'breakpoint-large-desktop': null,
		'breakpoint-large-mobile': null,
		'breakpoint-large-tablet': null,
		'breakpoint-macbook': null,
		'breakpoint-small-desktop': null,
		'breakpoint-small-mobile': null,
		'breakpoint-small-tablet': null,
		'color-black': null,
		'transition-duration': null,
		'transition-timing-function': null,
	};

	static eventBus = null;

	static isInitialized = false;

	static rootStyles = null;

	setup() {
		if (!App.isInitialized) {
			App.eventBus = new EventEmitter();

			App.rootStyles = getComputedStyle(document.documentElement);

			this.setupCSSVariables();

			App.isInitialized = true;
		}
	}

	setupCSSVariables() {
		App.cssVariables = Object.fromEntries(
			Object.keys(App.cssVariables).map((cssVariableKey) => {
				const cssVariableValue = App.rootStyles.getPropertyValue(`--${cssVariableKey}`);
				const parsedCssVariableValue =
					!Number.isNaN(window.parseInt(cssVariableValue, 0)) && !/(s|ms)$/gim.test(cssVariableValue)
						? window.parseInt(cssVariableValue)
						: cssVariableValue;

				return [cssVariableKey, parsedCssVariableValue];
			}),
		);
	}
}

export default App;
