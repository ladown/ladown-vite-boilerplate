import App from './app-scripts.js';

window.addEventListener('DOMContentLoaded', () => {
	new App().setup();
});
