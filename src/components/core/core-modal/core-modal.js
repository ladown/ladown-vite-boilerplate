import { disablePageScroll, enablePageScroll } from 'scroll-lock';

import elementTransitionEnd from '../../../scripts/shared/utils/elementTransitionEnd.js';
import getObjectLength from '../../../scripts/shared/utils/getObjectLength.js';
import isElementVisible from '../../../scripts/shared/utils/isElementVisible.js';
import reflow from '../../../scripts/shared/utils/reflow.js';
import sanitizeHTML from '../../../scripts/shared/utils/sanitizeHTML.js';

class CoreModal {
	constructor({ rootElement }) {
		this.modalId = rootElement.getAttribute('id');

		this.elements = {
			body: document.body,
			content: {},
			root: rootElement,
			wrapper: rootElement.querySelector(`.${CoreModal.classNames.wrapperClass}`) || rootElement,
		};

		this.settings = {
			hideAllModals: true,
			...JSON.parse(rootElement.getAttribute('data-core-modal-settings') || '{}'),
		};

		this.state = {
			hasCssAnimation: rootElement.classList.value.indexOf('animation_') !== -1 && this.elements.wrapper,
			isOpened: false,
		};

		if (!CoreModal.isDocumentListenerSet) {
			document.addEventListener('click', CoreModal.handleDocumentClick);

			CoreModal.isDocumentListenerSet = true;
		}
	}

	static classNames = {
		closeClass: 'js-core-modal-close',
		isActiveMode: 'is-active',
		isOpenedMode: 'is-opened',
		rootClass: 'js-core-modal',
		triggerClass: 'js-core-modal-trigger',
		wrapperClass: 'js-core-modal-wrapper',
	};

	static handleDocumentClick = (event) => {
		const pathTree = event.path || (event.composedPath && event.composedPath());
		const triggerElement = pathTree.find((element) => element?.classList?.contains(CoreModal.classNames.triggerClass));
		const triggerElementId = triggerElement?.getAttribute('data-core-modal-id');
		const modalInstance = CoreModal.instances[triggerElementId];

		if (triggerElement && modalInstance) {
			if (triggerElement.tagName.toLowerCase() === 'a') {
				event.preventDefault();
			}

			if (modalInstance.state.isOpened) {
				modalInstance.methods.closeModal();
			} else {
				modalInstance.methods.openModal({ triggerElement });
			}
		}
	};

	static hideAllModals = ({ id = '' } = {}) => {
		Object.keys(this.instances).forEach((modalId) => {
			if (this.instances[modalId]) {
				const modalInstance = this.instances[modalId];

				if (modalId !== id && modalInstance.state.isOpened) {
					modalInstance.methods.closeModal();
				}
			}
		});
	};

	static initialize = () => {
		const modals = document.querySelectorAll(`.${CoreModal.classNames.rootClass}`);

		modals.forEach((rootElement) => {
			const modalId = rootElement.getAttribute('id');

			if (modalId && !CoreModal.instances[modalId]) {
				new CoreModal({ rootElement }).init();
			}
		});
	};

	static instances = {};

	static isDocumentListenerSet = false;

	init() {
		this.listeners.addModalClickListener();

		CoreModal.instances[this.modalId] = this;
	}

	handlers = {
		handleDocumentKeydown: ({ code, key }) => {
			if (key === 'Escape' && code === 'Escape' && this.state.isOpened) {
				this.methods.closeModal();
			}
		},

		handleModalClick: (event) => {
			const pathTree = event.path || (event.composedPath && event.composedPath());

			if (
				pathTree.find((element) => element?.classList?.contains(CoreModal.classNames.closeClass)) &&
				this.state.isOpened
			) {
				this.methods.closeModal();
			} else if (
				pathTree.find(
					(element) => element.tagName?.toLowerCase() === 'a' || element.tagName?.toLowerCase() === 'button',
				) &&
				this.settings?.closeOnLinkClick &&
				this.state.isOpened
			) {
				this.methods.closeModal();
			}
		},
	};

	listeners = {
		addDocumentKeydownListener: () => {
			document.addEventListener('keydown', this.handlers.handleDocumentKeydown);
		},

		addModalClickListener: () => {
			this.elements.root.addEventListener('click', this.handlers.handleModalClick);
		},

		removeDocumentKeydownListener: () => {
			document.removeEventListener('keydown', this.handlers.handleDocumentKeydown);
		},
	};

	methods = {
		beforeClose: () => {
			this.methods.disableIframePlaying();
			this.listeners.removeDocumentKeydownListener();
			enablePageScroll();
		},

		beforeOpen: ({ triggerElement } = {}) => {
			if (triggerElement) {
				this.methods.setModalContent({ triggerElement });
			}

			this.listeners.addDocumentKeydownListener();

			if (this.settings.hideAllModals) {
				CoreModal.hideAllModals();
			}

			disablePageScroll();
		},

		closeModal: () => {
			this.methods.beforeClose();

			if (this.state.hasCssAnimation) {
				this.elements.body.style.setProperty('pointer-events', 'none');
				this.elements.root.classList.remove(CoreModal.classNames.isOpenedMode);

				elementTransitionEnd(this.elements.wrapper, () => {
					this.elements.root.style.setProperty('display', 'none');
					this.elements.body.style.removeProperty('pointer-events');
				});
			} else {
				this.elements.root.classList.remove(CoreModal.classNames.isOpenedMode);
			}

			this.state.isOpened = false;
		},

		disableIframePlaying: () => {
			if (this.elements.content.iframe) {
				this.elements.content.iframe.setAttribute('src', this.elements.content.iframe.getAttribute('src'));
			}
		},

		openModal: ({ triggerElement } = {}) => {
			this.methods.beforeOpen({ triggerElement });

			if (this.state.hasCssAnimation) {
				this.elements.body.style.setProperty('pointer-events', 'none');
				this.elements.root.style.setProperty('display', 'flex');

				reflow(this.elements.root);

				this.elements.root.classList.add(CoreModal.classNames.isOpenedMode);

				this.elements.body.style.setProperty('pointer-events', '');
			} else {
				this.elements.root.classList.add(CoreModal.classNames.isOpenedMode);
			}

			[...this.elements.root.querySelectorAll('input')]
				.filter((inputElement) => isElementVisible(inputElement))[0]
				?.focus({ preventScroll: true });

			this.state.isOpened = true;
		},

		setModalContent: ({ data, triggerElement } = {}) => {
			const content = triggerElement ? JSON.parse(triggerElement.getAttribute('data-core-modal-content') || '{}') : data;

			if (getObjectLength(content)) {
				Object.entries(content).forEach(([key, value]) => {
					Object.entries(value).forEach(([contentKey, contentValue]) => {
						if (this.elements.content[contentKey]) {
							if (key === 'textContent' && this.elements.content[contentKey].textContent !== contentValue) {
								this.elements.content[contentKey].textContent = contentValue;
							} else if (key === 'innerHTML') {
								this.elements.content[contentKey].innerHTML = sanitizeHTML(contentValue);
							} else if (key === 'img' && 'url' in contentValue) {
								this.elements.content[contentKey].setAttribute('src', contentValue.url);

								if ('alt' in contentValue) {
									this.elements.content[contentKey].setAttribute('alt', contentValue.alt);
								}
							}
						}
					});
				});
			}
		},
	};
}

export default CoreModal;
