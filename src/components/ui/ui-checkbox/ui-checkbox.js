const UiCheckbox = {
	data: {
		classes: {
			errorClass: 'js-ui-checkbox-error',
			inputClass: 'js-ui-checkbox-input',
			isErrorMode: 'is-error',
			isFilledMode: 'is-filled',
			isFocusedMode: 'is-focused',
		},

		elements: {
			root: document.querySelectorAll('.js-ui-checkbox'),
		},
	},

	handlers: {
		handleInputChange({ errorElement, inputElement, rootElement }) {
			const isValid = inputElement.checkValidity();

			this.methods.setState.call(this, { errorElement, inputElement, isValid, rootElement });
		},

		handleInputInValid({ errorElement, inputElement, rootElement }) {
			this.methods.setState.call(this, { errorElement, inputElement, isValid: false, rootElement });
		},

		handleInputValid({ errorElement, inputElement, rootElement }) {
			this.methods.setState.call(this, { errorElement, inputElement, isValid: true, rootElement });
		},
	},

	init() {
		if (this.data.elements.root.length) {
			this.data.elements.root.forEach((rootElement) => {
				const inputElement = rootElement.querySelector(`.${this.data.classes.inputClass}`);
				const errorElement = rootElement.querySelector(`.${this.data.classes.errorClass}`);

				if (inputElement) {
					this.listeners.setInputListeners.call(this, { errorElement, inputElement, rootElement });
				}
			});
		}
	},

	listeners: {
		setInputListeners({ errorElement, inputElement, rootElement }) {
			inputElement.addEventListener(
				'change',
				this.handlers.handleInputChange.bind(this, { errorElement, inputElement, rootElement }),
			);

			inputElement.addEventListener(
				'valid',
				this.handlers.handleInputValid.bind(this, { errorElement, inputElement, rootElement }),
			);

			inputElement.addEventListener(
				'invalid',
				this.handlers.handleInputInValid.bind(this, { errorElement, inputElement, rootElement }),
			);
		},
	},

	methods: {
		setState({ errorElement, inputElement, isValid, rootElement }) {
			if (isValid) {
				rootElement.classList.add(this.data.classes.isFilledMode);
				rootElement.classList.remove(this.data.classes.isErrorMode);
			} else {
				rootElement.classList.add(this.data.classes.isErrorMode);
				rootElement.classList.remove(this.data.classes.isFilledMode);
				errorElement.textContent = inputElement.validationMessage;
			}
		},
	},
};

export default UiCheckbox;
